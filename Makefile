pdf: 
	pandoc --toc graphs.pst -o graphs.pdf

html: 
	mkdir -p public 
	pandoc --toc -o index.html graphs.pst
	cp index.html public/index.html

all: pdf html 

clean: 
	rm -f graphs.pdf 
	rm -rf public

