# Notes for the course MA010

The notes are written as a markdown file which is compiled using pandoc.

Built pdf version can be downloaded [here](https://gitlab.fi.muni.cz/xkucerak/ma010-notes/-/jobs/artifacts/main/raw/graphs.pdf?job=documents)

Built html version can be viewed [here](https://xkucerak.pages.fi.muni.cz/ma010-notes/). 
The built version is scuffed sometimes as the formula renderer has some issues.
